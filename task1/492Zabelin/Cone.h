#pragma once

#include <glm/glm.hpp>
#include <Mesh.hpp>


struct ConeParams {
    glm::vec3 firstPoint;
    glm::vec3 secondPoint;
    float thickness;
    float thicknessScale;
};


MeshPtr getCone(const ConeParams& params);
