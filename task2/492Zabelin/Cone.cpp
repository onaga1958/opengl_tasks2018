#define _USE_MATH_DEFINES

#include "Cone.h"

#include <iostream>
#include <vector>
#include <math.h>
#include <cmath>
#include <GL/glew.h>
#include <glm/gtc/matrix_transform.hpp>


void pushTriangle(
    std::vector<glm::vec3>& vertices,
    std::vector<glm::vec3>& normals,
    std::vector<glm::vec2>& texcoords,
    const std::vector<glm::vec3>& triangleVertices,
    const std::vector<std::vector<float>>& triangleTexcoords
) {
    glm::vec3 normal = glm::cross(
        triangleVertices[1] - triangleVertices[0],
        triangleVertices[2] - triangleVertices[0]
    );
    normal /= glm::length(normal);

    for (const auto& vertex: triangleVertices) {
        vertices.push_back(vertex);
        normals.push_back(normal);
    }

    for (int i = 0; i < 3; ++i)
        texcoords.push_back(glm::vec2(triangleTexcoords[i][0], triangleTexcoords[i][1]));
}


MeshPtr makeDefaultCone(
    float bigRadius,
    float smallRadius,
    float height,
    size_t trianglesNum = 50
) {
    std::vector<glm::vec3> vertices;
    std::vector<glm::vec3> normals;
    std::vector<glm::vec2> texcoords;

    size_t pointsOnCircle = trianglesNum / 2;

    float sectorAngle = 2 * M_PI / pointsOnCircle;

    for (size_t step = 0; step < pointsOnCircle; ++step) {
        glm::vec3 firstPointOnLowerBase(
            0,
            std::cos(step * sectorAngle) * bigRadius,
            std::sin(step * sectorAngle) * bigRadius
        );
        glm::vec3 secondPointOnLowerBase(
            0,
            std::cos((step + 1) * sectorAngle) * bigRadius,
            std::sin((step + 1) * sectorAngle) * bigRadius
        );
        glm::vec3 firstPointOnUpperBase(
            height,
            std::cos(step * sectorAngle) * smallRadius,
            std::sin(step * sectorAngle) * smallRadius
        );
        glm::vec3 secondPointOnUpperBase(
            height,
            std::cos((step + 1) * sectorAngle) * smallRadius,
            std::sin((step + 1) * sectorAngle) * smallRadius
        );
        float texcoord_0 = static_cast<float>(step) / pointsOnCircle;
        float texcoord_1 = static_cast<float>(step + 1) / pointsOnCircle;
        pushTriangle(
            vertices,
            normals,
            texcoords,
            {
                firstPointOnLowerBase,
                secondPointOnLowerBase,
                firstPointOnUpperBase
            },
            {{texcoord_0, 0}, {texcoord_1, 0}, {texcoord_0, 1}}
        );
        pushTriangle(
            vertices,
            normals,
            texcoords,
            {
                secondPointOnLowerBase,
                secondPointOnUpperBase,
                firstPointOnUpperBase
            },
            {{texcoord_1, 0}, {texcoord_1, 1}, {texcoord_0, 1}}
        );
    }

    DataBufferPtr verteciesBuffer = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    verteciesBuffer->setData(vertices.size() * sizeof(float) * 3, vertices.data());

    DataBufferPtr normalsBuffer = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    normalsBuffer->setData(normals.size() * sizeof(float) * 3, normals.data());

    DataBufferPtr textureBuffer = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    textureBuffer->setData(texcoords.size() * sizeof(float) * 2, texcoords.data());

    MeshPtr mesh = std::make_shared<Mesh>();
    mesh->setAttribute(0, 3, GL_FLOAT, GL_FALSE, 0, 0, verteciesBuffer);
    mesh->setAttribute(1, 3, GL_FLOAT, GL_FALSE, 0, 0, normalsBuffer);
    mesh->setAttribute(2, 2, GL_FLOAT, GL_FALSE, 0, 0, textureBuffer);
    mesh->setPrimitiveType(GL_TRIANGLES);
    mesh->setVertexCount(vertices.size());

    return mesh;
}

glm::vec3 swapAxis(glm::vec3 point) {
    return glm::vec3(point.z, point.x, point.y);
}

MeshPtr getCone(const ConeParams& params) {
    float bigRadius = params.thickness;
    float smallRadius = params.thickness * params.thicknessScale;

    glm::vec3 firstPoint = swapAxis(params.firstPoint);
    glm::vec3 secondPoint = swapAxis(params.secondPoint);
    float height = glm::distance(firstPoint, secondPoint);
    MeshPtr cone = makeDefaultCone(bigRadius, smallRadius, height);

    glm::mat4 modelMatrix = glm::translate(glm::mat4(1.0f), firstPoint);
    float cos = (secondPoint.x - firstPoint.x) / height;
    float angle = std::acos(cos);
    glm::vec3 axis = glm::cross(glm::vec3(1, 0, 0), secondPoint - firstPoint);
    modelMatrix = glm::rotate(modelMatrix, angle, axis);
    cone->setModelMatrix(modelMatrix);

    return cone;
}
