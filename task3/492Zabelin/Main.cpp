#include "TreeApplication.h"


int main()
{
    TreeApplication app;
    std::map<char, std::string> rules;
    // rules['F'] = "F[-F]<[F]>[+F][F]";
    rules['F'] = "F[-F][^F][&F][+F][F]";
    // rules['F'] = "F[-F]F[+F][F]";
    std::string initialString = "F";
    float stepDistance = 0.7;
    float rotateAngle = 50;
    float thickness = 0.07;
    float distanceScale = 0.6;
    float angleScale = 0.9;
    float thicknessScale = 0.6;
    uint16_t numIterations = 5;

    LSystem system(
        rules,
        initialString,
        stepDistance,
        rotateAngle,
        thickness,
        distanceScale,
        angleScale,
        thicknessScale
    );
    app.setupLSystem(
        system,
        numIterations
    );

    float leafThicknessBound = 0.01;
    float leafWidth = 0.03;
    float leafLength = 0.05;
    size_t leafNum = 7;
    app.setLeafParams(
        leafThicknessBound,
        leafWidth,
        leafLength,
        leafNum
    );
    app.setTreeCount(500);

    //app.setDebug(true);

    app.start();
    return 0;
}
