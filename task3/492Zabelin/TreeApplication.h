#pragma once

#include <iostream>
#include <random>
#include <vector>

#include <Application.hpp>
#include <Common.h>
#include <Mesh.hpp>
#include <ShaderProgram.hpp>
#include <Texture.hpp>

#include "LSystem.h"

struct MaterialInfo {
    glm::vec3 Ka;
    glm::vec3 Kd;
    glm::vec3 Ks;
};

class TreeApplication : public Application {
    std::vector<glm::vec3> _positionsVec3;
    DataBufferPtr _bufVec3;

	TexturePtr _bufferTexCulled;

	GLuint _TF;
	GLuint _cullVao;
	GLuint _tfOutputVbo;
	GLuint _query;

    unsigned int treeCount = 50;
    int xSquares = 25;
    int ySquares = 25;
    float bound = 25;

    bool debug;

    ShaderProgramPtr cullShader;
    ShaderProgramPtr shader;
    ShaderProgramPtr groundShader;
    ShaderProgramPtr leafShader; // without normal mapping

    std::mt19937 gen;
    std::uniform_real_distribution<float> angleDistribution;
    std::uniform_real_distribution<float> spaceDistribution;
    std::uniform_real_distribution<float> xPositionDistribution;
    std::uniform_real_distribution<float> yPositionDistribution;

    //Координаты источника света
    float _phi = 0.0f;
    float _theta = glm::pi<float>() * 0.25f;

    //Параметры источника света
    glm::vec3 _lightAmbientColor;
    glm::vec3 _lightDiffuseColor;
    glm::vec3 _lightSpecularColor;

    MaterialInfo leafMaterial;
    MaterialInfo barkMaterial;
    MaterialInfo grassMaterial;

    std::vector<ConeParams> conesParams;
    MeshData allConesData;
    MeshData allLeavesData;
    MeshPtr leavesMesh;
    MeshPtr groundMesh;
    MeshPtr trunkMesh;

    TexturePtr barkTexture;
    TexturePtr normalMapTexture;
    TexturePtr leafTexture;
    TexturePtr groundTexture;

    GLuint barkSampler;
    GLuint normalMapSampler;
    GLuint leafSampler;
    GLuint groundSampler;

    float leafThicknessBound;
    float leafLength;
    float leafWidth;
    size_t leafNum;
private:
    void log(const std::string& str);
    MeshPtr getMesh(const MeshData& data);
    void addCone(const ConeParams& params);
    void addLeaf(const ConeParams& params, bool drawOnTop);
    void setGround();
public:
    void setDebug(bool debug);
    void setTreeCount(unsigned int treeCount);
    void setLeafParams(
        float thicknessBound,
        float width,
        float length,
        size_t leafNumber
    );
    void setupLSystem(
        LSystem& system,
        uint16_t numIterations
    );
    void makeScene() override;
    void draw() override;
    void updateGUI() override;
};
