#include "TreeApplication.h"
#include "Parallelepiped.h"
#include <string>
#include <iostream>
#include <exception>
#include <stdlib.h>

void printVec(const glm::vec3& a, std::string name = "") {
    std::cout << name << " " << a.x << " " << a.y << " " << a.z << std::endl;
}

void TreeApplication::setDebug(bool debug) {
    this->debug = debug;
}

void TreeApplication::setTreeCount(unsigned int treeCount) {
    this->treeCount = treeCount;
}

void TreeApplication::setupLSystem(
    LSystem& system,
    uint16_t numIterations
) {
    system.executeIterations(numIterations);
    conesParams = system.getConesParams();
}

void TreeApplication::setLeafParams(
    float thicknessBound,
    float width,
    float length,
    size_t leafNumber
) {
    gen = std::mt19937(111);
    angleDistribution = std::uniform_real_distribution<float>(-M_PI, M_PI);
    spaceDistribution = std::uniform_real_distribution<float>(0, 1);

    leafThicknessBound = thicknessBound;
    leafWidth = width;
    leafLength = length;
    leafNum = leafNumber;
}

void TreeApplication::addCone(const ConeParams& params) {
    MeshData data = getCone(params);
    allConesData.vertices.insert(
        allConesData.vertices.end(),
        data.vertices.begin(),
        data.vertices.end()
    );
    allConesData.normals.insert(
        allConesData.normals.end(),
        data.normals.begin(),
        data.normals.end()
    );
    allConesData.texCords.insert(
        allConesData.texCords.end(),
        data.texCords.begin(),
        data.texCords.end()
    );
}

void TreeApplication::addLeaf(
    const ConeParams& params,
    bool drawOnTop
) {
    glm::vec3 tmp(0, 0, 0);
    glm::vec3 differ = params.secondPoint - params.firstPoint;
    if (differ.x)
        tmp.z = 1;
    else
        tmp.x = 1;
    glm::vec3 direction = glm::normalize(glm::cross(tmp, differ));

    float alpha = 0.01;
    if (!drawOnTop)
        alpha = spaceDistribution(gen);
    glm::vec3 barkAxis = glm::normalize(params.secondPoint - params.firstPoint);
    glm::vec3 petioleShift = barkAxis * float(leafWidth * 0.2);

    glm::vec3 firstPoint = alpha * params.firstPoint + (1 - alpha) * params.secondPoint;

    float realThickness = params.thickness * (alpha + params.thicknessScale * (1 - alpha));
    realThickness -= leafLength * 0.15;
    firstPoint += direction * realThickness;

    glm::vec3 secondPoint = firstPoint + direction * leafLength;
    glm::vec3 normal = glm::normalize(glm::cross(barkAxis, secondPoint - firstPoint));
    glm::vec3 center = alpha * params.firstPoint + (1 - alpha) * params.secondPoint;
    float angleAroundBark = angleDistribution(gen);
    float angleAroundSelfAxis = angleDistribution(gen);

    std::vector<glm::vec3> leaf = {
        firstPoint + barkAxis * (leafWidth / 2),
        firstPoint - barkAxis * (leafWidth / 2),
        secondPoint - barkAxis * (leafWidth / 2),
        firstPoint + barkAxis * (leafWidth / 2),
        secondPoint - barkAxis * (leafWidth / 2),
        secondPoint + barkAxis * (leafWidth / 2)
    };
    std::vector<glm::vec2> leafTextureCoords = {
        glm::vec2(0, 0),
        glm::vec2(0, 1),
        glm::vec2(1, 1),
        glm::vec2(0, 0),
        glm::vec2(1, 1),
        glm::vec2(1, 0),
    };

    glm::mat4 modelMatrix;
    modelMatrix = glm::translate(glm::mat4(1.0f), center);
    modelMatrix = glm::rotate(modelMatrix, angleAroundBark, barkAxis);
    modelMatrix = glm::translate(modelMatrix, firstPoint - center + petioleShift);
    modelMatrix = glm::rotate(modelMatrix, angleAroundSelfAxis, direction);
    modelMatrix = glm::translate(modelMatrix, -firstPoint - petioleShift);

    normal = getVec3((glm::transpose(glm::inverse(modelMatrix)) * glm::vec4(normal, 1)));
    for (int i = 0; i < 6; ++i) {
        allLeavesData.vertices.push_back(swapAxis(getVec3(modelMatrix * glm::vec4(leaf[i], 1))));
        allLeavesData.normals.push_back(swapAxis(normal));
        allLeavesData.texCords.push_back(leafTextureCoords[i]);
    }
}

void TreeApplication::setGround() {
    MeshData data;
    data.vertices = {
        glm::vec3(-bound, -bound, -0.8),
        glm::vec3(bound, -bound, -0.8),
        glm::vec3(bound, bound, -0.8),
        glm::vec3(-bound, -bound, -0.8),
        glm::vec3(bound, bound, -0.8),
        glm::vec3(-bound, bound, -0.8)
    };
    data.normals = {
        glm::vec3(0, 0, 1),
        glm::vec3(0, 0, 1),
        glm::vec3(0, 0, 1),
        glm::vec3(0, 0, 1),
        glm::vec3(0, 0, 1),
        glm::vec3(0, 0, 1)
    };
    data.texCords = {
        glm::vec2(0, 0),
        glm::vec2(float(xSquares), 0),
        glm::vec2(float(xSquares), float(ySquares)),
        glm::vec2(0, 0),
        glm::vec2(float(xSquares), float(ySquares)),
        glm::vec2(0, float(ySquares))
    };

    groundMesh = getMesh(data);
}

void TreeApplication::log(const std::string& str) {
    if (debug)
        std::cout << "[DEBUG LOG]: " << str << std::endl;
}

MeshPtr TreeApplication::getMesh(const MeshData& data) {
    if (data.vertices.size() != data.normals.size()
            or data.vertices.size() != data.texCords.size())
        throw std::invalid_argument("vertices, normals and texCords sizes mast be equal");

    int size = data.vertices.size();
    log("size: " + std::to_string(size));

    log("create vertices buffer");
    log("vertices size: " + std::to_string(data.vertices.size()));
    DataBufferPtr verteciesBuffer = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    verteciesBuffer->setData(size * sizeof(float) * 3, data.vertices.data());

    log("create normals buffer");
    log("normal size: " + std::to_string(data.normals.size()));
    DataBufferPtr normalsBuffer = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    normalsBuffer->setData(size * sizeof(float) * 3, data.normals.data());

    log("create texture buffer");
    log("texCords size: " + std::to_string(data.texCords.size()));
    DataBufferPtr textureBuffer = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    textureBuffer->setData(size * sizeof(float) * 2, data.texCords.data());

    MeshPtr mesh = std::make_shared<Mesh>();
    log("set vertices buffer");
    mesh->setAttribute(0, 3, GL_FLOAT, GL_FALSE, 0, 0, verteciesBuffer);
    log("set normals buffer");
    mesh->setAttribute(1, 3, GL_FLOAT, GL_FALSE, 0, 0, normalsBuffer);
    log("set texture buffer");
    mesh->setAttribute(2, 2, GL_FLOAT, GL_FALSE, 0, 0, textureBuffer);
    mesh->setPrimitiveType(GL_TRIANGLES);
    mesh->setVertexCount(size);
    mesh->setModelMatrix(glm::mat4(1));
    return mesh;
}

void TreeApplication::makeScene()
{
    Application::makeScene();
    xPositionDistribution = std::uniform_real_distribution<float>(-bound * 0.9, bound * 0.9);
    yPositionDistribution = std::uniform_real_distribution<float>(-bound * 0.9, bound * 0.9);
    for (int i = 0; i < treeCount; ++i) {
        float x = xPositionDistribution(gen);
        float y = yPositionDistribution(gen);
        _positionsVec3.push_back(glm::vec3(x, y, 0.0));
    }

    _bufVec3 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    _bufVec3->setData(_positionsVec3.size() * sizeof(float) * 3, _positionsVec3.data());

	glGenBuffers(1, &_tfOutputVbo);
	glBindBuffer(GL_ARRAY_BUFFER, _tfOutputVbo);
	glBufferData(GL_ARRAY_BUFFER, _positionsVec3.size() * sizeof(float) * 3, 0, GL_STREAM_DRAW);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	_bufferTexCulled = std::make_shared<Texture>(GL_TEXTURE_BUFFER);
	_bufferTexCulled->bind();
	glTexBuffer(GL_TEXTURE_BUFFER, GL_RGB32F_ARB, _tfOutputVbo);
	_bufferTexCulled->unbind();

    log("add leaves and cones");
    for (const auto& params: conesParams) {
        addCone(params);
        if (params.thickness < leafThicknessBound)
            for (size_t leafInd = 0; leafInd < leafNum; ++leafInd)
                addLeaf(params, leafInd < 2);
    }
    log("create leaves mesh");
    leavesMesh = getMesh(allLeavesData);
    log("create trunk mesh");
    trunkMesh = getMesh(allConesData);
    log("create ground mesh");
    setGround();
    log("all meshs were created");

    // Инициализируем параметры материалов
    glm::vec3 brown(1, 92. / 117, 72. / 117);
    barkMaterial.Ka = brown;
    barkMaterial.Kd = brown;
    barkMaterial.Ks = brown / float(2.);
    leafMaterial.Ka = glm::vec3(1);
    leafMaterial.Kd = glm::vec3(1);
    leafMaterial.Ks = glm::vec3(1. / 5);
    grassMaterial.Ka = glm::vec3(1);
    grassMaterial.Kd = glm::vec3(1);
    grassMaterial.Ks = glm::vec3(1. / 5);

    log("cull shader setup");
    cullShader = std::make_shared<ShaderProgram>();

	ShaderPtr vs = std::make_shared<Shader>(GL_VERTEX_SHADER);
	vs->createFromFile("492ZabelinData/cull.vert");
	cullShader->attachShader(vs);

	ShaderPtr gs = std::make_shared<Shader>(GL_GEOMETRY_SHADER);
	gs->createFromFile("492ZabelinData/cull.geom");
	cullShader->attachShader(gs);

	const char* attribs[] = { "position" };
	glTransformFeedbackVaryings(cullShader->id(), 1, attribs, GL_SEPARATE_ATTRIBS);

	cullShader->linkProgram();

	//VAO, который будет поставлять данные для отсечения
	glGenVertexArrays(1, &_cullVao);
	glBindVertexArray(_cullVao);

	glEnableVertexAttribArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, _bufVec3->id());
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0);

	glBindVertexArray(0);

	//Настроечный объект Transform Feedback
    log("transform feedback setup");
	glGenTransformFeedbacks(1, &_TF);
	glBindTransformFeedback(GL_TRANSFORM_FEEDBACK, _TF);
	glBindBufferBase(GL_TRANSFORM_FEEDBACK_BUFFER, 0, _tfOutputVbo);
	glBindTransformFeedback(GL_TRANSFORM_FEEDBACK, 0);

	glGenQueries(1, &_query);

    log("main shaders setup");
    shader = std::make_shared<ShaderProgram>(
        "492ZabelinData/shader.vert",
        "492ZabelinData/shaderWithNormalMap.frag"
    );

    groundShader = std::make_shared<ShaderProgram>(
        "492ZabelinData/groundShader.vert",
        "492ZabelinData/groundShader.frag"
    );

    leafShader = std::make_shared<ShaderProgram>(
        "492ZabelinData/shader.vert",
        "492ZabelinData/leafShader.frag"
    );

    //Инициализация значений переменных освщения
    _lightAmbientColor = glm::vec3(0.2, 0.2, 0.2);
    _lightDiffuseColor = glm::vec3(0.8, 0.8, 0.8);
    _lightSpecularColor = glm::vec3(1, 1, 1);

    // Загрузка и создание текстур
    barkTexture = loadTexture("492ZabelinData/wood_texture.jpg");
    normalMapTexture = loadTexture("492ZabelinData/wood_normal_map.jpg");
    leafTexture = loadTexture("492ZabelinData/leaf_alphamasked.png");
    groundTexture = loadTexture("492ZabelinData/grass_texture.jpg");

    // Инициализация сэмплеров
    glGenSamplers(1, &barkSampler);

    glSamplerParameteri(barkSampler, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glSamplerParameteri(barkSampler, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
    glSamplerParameteri(barkSampler, GL_TEXTURE_WRAP_S, GL_REPEAT);

    glGenSamplers(1, &normalMapSampler);
    glSamplerParameteri(normalMapSampler, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glSamplerParameteri(normalMapSampler, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
    glSamplerParameteri(normalMapSampler, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glSamplerParameteri(normalMapSampler, GL_TEXTURE_WRAP_T, GL_REPEAT);

    glGenSamplers(1, &leafSampler);
    glSamplerParameteri(leafSampler, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glSamplerParameteri(leafSampler, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
    glSamplerParameteri(leafSampler, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glSamplerParameteri(leafSampler, GL_TEXTURE_WRAP_T, GL_REPEAT);

    glGenSamplers(1, &groundSampler);
    glSamplerParameteri(groundSampler, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glSamplerParameteri(groundSampler, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
    glSamplerParameteri(groundSampler, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glSamplerParameteri(groundSampler, GL_TEXTURE_WRAP_T, GL_REPEAT);
}

void TreeApplication::updateGUI()
{
    Application::updateGUI();

    ImGui::SetNextWindowPos(ImVec2(0, 0), ImGuiSetCond_FirstUseEver);
    if (ImGui::Begin("MIPT OpenGL Sample", NULL, ImGuiWindowFlags_AlwaysAutoResize))
    {
        ImGui::Text("FPS %.1f", ImGui::GetIO().Framerate);

        if (ImGui::CollapsingHeader("Light"))
        {
            ImGui::ColorEdit3("ambient", glm::value_ptr(_lightAmbientColor));
            ImGui::ColorEdit3("diffuse", glm::value_ptr(_lightDiffuseColor));
            ImGui::ColorEdit3("specular", glm::value_ptr(_lightSpecularColor));

            ImGui::SliderFloat("phi", &_phi, 0.0f, 2.0f * glm::pi<float>());
            ImGui::SliderFloat("theta", &_theta, 0.0f, glm::pi<float>());
        }

        if (ImGui::CollapsingHeader("Bark material"))
        {
            ImGui::ColorEdit3("bark ambient", glm::value_ptr(barkMaterial.Ka));
            ImGui::ColorEdit3("bark diffuse", glm::value_ptr(barkMaterial.Kd));
            ImGui::ColorEdit3("bark specular", glm::value_ptr(barkMaterial.Ks));
        }

        if (ImGui::CollapsingHeader("Grass material"))
        {
            ImGui::ColorEdit3("grass ambient", glm::value_ptr(grassMaterial.Ka));
            ImGui::ColorEdit3("grass diffuse", glm::value_ptr(grassMaterial.Kd));
            ImGui::ColorEdit3("grass specular", glm::value_ptr(grassMaterial.Ks));
        }

        if (ImGui::CollapsingHeader("Leaf material"))
        {
            ImGui::ColorEdit3("leaf ambient", glm::value_ptr(leafMaterial.Ka));
            ImGui::ColorEdit3("leaf diffuse", glm::value_ptr(leafMaterial.Kd));
            ImGui::ColorEdit3("leaf specular", glm::value_ptr(leafMaterial.Ks));
        }
    }
    ImGui::End();
}


void TreeApplication::draw()
{
    Application::draw();

    //Получаем размеры экрана (окна)
    int width, height;
    glfwGetFramebufferSize(_window, &width, &height);

    //Устанавливаем порт вывода на весь экран (окно)
    glViewport(0, 0, width, height);

    //Очищаем порт вывода (буфер цвета и буфер глубины)
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    log("prepare culling");
	cullShader->use();
    cullShader->setMat4Uniform("toCameraMatrix", _camera.viewMatrix);
	glEnable(GL_RASTERIZER_DISCARD);
	glBindTransformFeedback(GL_TRANSFORM_FEEDBACK, _TF);
	glBeginQuery(GL_TRANSFORM_FEEDBACK_PRIMITIVES_WRITTEN, _query);
	glBeginTransformFeedback(GL_POINTS);

    log("get arrays");
	glBindVertexArray(_cullVao);
	glDrawArrays(GL_POINTS, 0, _positionsVec3.size());

	glEndTransformFeedback();
	glEndQuery(GL_TRANSFORM_FEEDBACK_PRIMITIVES_WRITTEN);

    glDisable(GL_RASTERIZER_DISCARD);

    log("get primitives written");
	GLuint primitivesWritten;
	glGetQueryObjectuiv(_query, GL_QUERY_RESULT, &primitivesWritten);

    //Устанавливаем шейдер.
    log("setup common params");
    shader->use();
    //Устанавливаем общие юниформ-переменные
    glm::vec3 lightDir = glm::vec3(
        glm::cos(_phi) * glm::cos(_theta),
        glm::sin(_phi) * glm::cos(_theta),
        glm::sin(_theta)
    );
    shader->setMat4Uniform("projectionMatrix", _camera.projMatrix);
    shader->setMat4Uniform("viewMatrix", _camera.viewMatrix);

    shader->setVec3Uniform("light.dir", lightDir);
    shader->setVec3Uniform("light.La", _lightAmbientColor);
    shader->setVec3Uniform("light.Ld", _lightDiffuseColor);
    shader->setVec3Uniform("light.Ls", _lightSpecularColor);


    shader->setVec3Uniform("material.Ka", barkMaterial.Ka);
    shader->setVec3Uniform("material.Kd", barkMaterial.Kd);
    shader->setVec3Uniform("material.Ks", barkMaterial.Ks);

    // Почему-то если barkUnit = 0, то GUI не отображается
    log("send bark texture to GPU");
    GLuint barkUnit = 3;
    glBindTextureUnit(barkUnit, barkTexture->texture());
    glBindSampler(barkUnit, barkSampler);
    shader->setIntUniform("diffuseTexture", barkUnit);

	glActiveTexture(GL_TEXTURE5);
	_bufferTexCulled->bind();
	shader->setIntUniform("texBuf", 5);

    log("send normal texture to GPU");
    GLuint normalMapUnit = 1;
    glBindTextureUnit(normalMapUnit, normalMapTexture->texture());
    glBindSampler(normalMapUnit, normalMapSampler);
    shader->setIntUniform("normalMapTexture", normalMapUnit);


    glm::mat3 normalToCameraMatrix = glm::transpose(glm::inverse(glm::mat3(_camera.viewMatrix)));

    //Рисуем дерево
    log("draw trunk");
    shader->setMat4Uniform("modelMatrix", glm::mat4(1));
    glm::mat4 toCameraMatrix = _camera.viewMatrix;
    shader->setMat4Uniform("toCameraMatrix", toCameraMatrix);
    shader->setMat3Uniform("normalToCameraMatrix", normalToCameraMatrix);
    trunkMesh->drawInstanced(primitivesWritten);

    log("draw ground");
    groundShader->use();
    groundShader->setMat4Uniform("projectionMatrix", _camera.projMatrix);
    groundShader->setMat4Uniform("viewMatrix", _camera.viewMatrix);

    groundShader->setVec3Uniform("light.dir", lightDir);
    groundShader->setVec3Uniform("light.La", _lightAmbientColor);
    groundShader->setVec3Uniform("light.Ld", _lightDiffuseColor);
    groundShader->setVec3Uniform("light.Ls", _lightSpecularColor);

    groundShader->setVec3Uniform("material.Ka", grassMaterial.Ka);
    groundShader->setVec3Uniform("material.Kd", grassMaterial.Kd);
    groundShader->setVec3Uniform("material.Ks", grassMaterial.Ks);

    GLuint groundUnit = 4;
    glBindSampler(groundUnit, groundSampler);
    glBindTextureUnit(groundUnit, groundTexture->texture());
    groundShader->setMat4Uniform("modelMatrix", glm::mat4(1));
    groundShader->setIntUniform("diffuseTexture", groundUnit);
    groundShader->setMat4Uniform("toCameraMatrix", _camera.viewMatrix);
    groundShader->setMat3Uniform("normalToCameraMatrix", normalToCameraMatrix);
    groundMesh->draw();

    //Устанавливаем шейдер.
    leafShader->use();
    // glShaderStorageBlockBinding(leafShader->id(), ssboIndex, 0); //0я точка привязки

    //Устанавливаем общие юниформ-переменные
    leafShader->setMat4Uniform("modelMatrix", glm::mat4(1));
    leafShader->setMat4Uniform("projectionMatrix", _camera.projMatrix);
    leafShader->setMat4Uniform("viewMatrix", _camera.viewMatrix);

    leafShader->setVec3Uniform("light.dir", lightDir);
    leafShader->setVec3Uniform("light.La", _lightAmbientColor);
    leafShader->setVec3Uniform("light.Ld", _lightDiffuseColor);
    leafShader->setVec3Uniform("light.Ls", _lightSpecularColor);

    leafShader->setVec3Uniform("material.Ka", leafMaterial.Ka);
    leafShader->setVec3Uniform("material.Kd", leafMaterial.Kd);
    leafShader->setVec3Uniform("material.Ks", leafMaterial.Ks);

	glActiveTexture(GL_TEXTURE5);
	_bufferTexCulled->bind();
	leafShader->setIntUniform("texBuf", 5);

    log("send leaves texture to GPU");
    GLuint leafUnit = 2;
    glBindSampler(leafUnit, leafSampler);
    glBindTextureUnit(leafUnit, leafTexture->texture());
    leafShader->setIntUniform("diffuseTexture", leafUnit);

    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    log("draw leaves");
    leafShader->setMat4Uniform("toCameraMatrix", _camera.viewMatrix);
    leafShader->setMat3Uniform("normalToCameraMatrix", normalToCameraMatrix);
    leavesMesh->drawInstanced(primitivesWritten);

    glBindSampler(0, 0);
	glUseProgram(0);
}
