#pragma once

#include <glm/glm.hpp>
#include <Mesh.hpp>
#include <vector>


struct ConeParams {
    glm::vec3 firstPoint;
    glm::vec3 secondPoint;
    float thickness;
    float thicknessScale;
};


struct MeshData {
    std::vector<glm::vec3> vertices;
    std::vector<glm::vec3> normals;
    std::vector<glm::vec2> texCords;
};


glm::vec3 getVec3(const glm::vec4& a);
glm::vec3 swapAxis(glm::vec3 point);
MeshData getCone(const ConeParams& params);
