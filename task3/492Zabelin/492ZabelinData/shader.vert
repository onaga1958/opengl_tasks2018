#version 430 core

uniform samplerBuffer texBuf;

uniform mat4 modelMatrix;
uniform mat4 toCameraMatrix;
uniform mat3 normalToCameraMatrix;
uniform mat4 projectionMatrix;

layout(location = 0) in vec3 vertexPosition;
layout(location = 1) in vec3 vertexNormal;
layout(location = 2) in vec2 vertexTextureCoords;

// в системе координат камеры
out vec3 normal;

// в системе координат камеры
out vec3 position;
out vec2 textureCoords;

void main() {
	vec3 modelPos = texelFetch(texBuf, gl_InstanceID).rgb;

    textureCoords = vertexTextureCoords;
    vec4 _positionVec4 = toCameraMatrix * (modelMatrix * vec4(vertexPosition + modelPos, 1.0));
    position = _positionVec4.xyz;
    normal = normalToCameraMatrix * vertexNormal;
    gl_Position = projectionMatrix * _positionVec4;
}
