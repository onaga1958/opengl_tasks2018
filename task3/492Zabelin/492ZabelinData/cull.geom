#version 430

layout(points) in;
layout(points, max_vertices = 1) out;

uniform mat4 toCameraMatrix;

out vec3 position;

void main()
{
    if (length(toCameraMatrix * vec4(gl_in[0].gl_Position.xyz, 1.0)) < 20.0)
    {
        position = gl_in[0].gl_Position.xyz;

        EmitVertex();
        EndPrimitive();
    }
}
